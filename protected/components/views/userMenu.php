<html>
<head>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" />
</head>
</head>
<body>
	<div id="usermenu">
		<ul>
		    <li><?php echo CHtml::link('Create New Post',array('post/create')); ?></li>
		    <li><?php echo CHtml::link('Manage Posts',array('post/admin')); ?></li>
		    <li><?php  echo CHtml::link('Approve Comments',array('comment/index')). ' (' . Comment::model()->pendingCommentCount . ')'; ?></li>
		    <li><?php echo CHtml::link('Logout',array('site/logout')); ?></li>
		</ul>
	</div>
</body>
</html>