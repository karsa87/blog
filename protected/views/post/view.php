<head>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" />
</head>

<?php
/* @var $this PostController */
/* @var $model Post */

$this->breadcrumbs=array(
	'Posts'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Post', 'url'=>array('index')),
	array('label'=>'Create Post', 'url'=>array('create')),
	array('label'=>'Update Post', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Post', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Post', 'url'=>array('admin')),
);
?>

<h1>View Post #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'content',
		'tags',
		'status',
		'create_time',
		'update_time',
		'author_id',
	),
)); ?>


<div id="comments">
    <?php 
	    if($model->commentCount>=1){
	    	echo "<h3>";
			echo CHtml::link($model->commentCount . ' comment(s)', array('comment/index','post'=>$model->id));
	    	echo "</h3>";

	    	/*

	    	masih belum tahu bagaimana isinya 
	    	$this->renderPartial('_comments',array(
	            'post'=>$model,
	            'comments'=>$model->comments,
        	));
        	*/
	    }
    ?>

    <h3>Leave a Comment</h3>
 
    <?php if(Yii::app()->user->hasFlash('commentSubmitted')): ?>
        <div class="flash-success">
            <?php echo Yii::app()->user->getFlash('commentSubmitted'); ?>
        </div>
    <?php else: ?>
    	<div class="flash-success">
	        <?php $this->renderPartial('/comment/_form',array(
	            'model'=>$comment,
	        )); ?>
        </div>
    <?php endif; ?>
</div><!-- comments -->

<!-- Error 500

Undefined variable: dataProvider
	<?php  /* if(!empty($_GET['tag'])): ?>
	<h1>Posts Tagged with <i><?php echo CHtml::encode($_GET['tag']); ?></i></h1>
	<?php endif; */ ?>
	 
	<?php  /* $this->widget('zii.widgets.CListView', array(
	    'dataProvider'=>$dataProvider,
	    'itemView'=>'_view',
	    'template'=>"{items}\n{pager}",
	)); */ ?>
-->