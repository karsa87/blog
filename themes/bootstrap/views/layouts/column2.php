<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="row">
    <div class="span9">
        <div id="content">
            <?php echo $content; ?>
        </div><!-- content -->
    </div>
    <div class="span3">
        <div id="sidebar">
        <?php $this->widget('TagCloud', array(
            'maxTags'=>Yii::app()->params['tagCloudCount'],
        )); ?>

        <?php $this->widget('RecentComments', array(
            'maxComments'=>Yii::app()->params['recentCommentCount'],
        )); ?>

        <?php
            $this->beginWidget('zii.widgets.CPortlet', array(
                'title'=>'Operations',
            ));
            $this->widget('bootstrap.widgets.TbMenu', array(
                'items'=>$this->menu,
                'htmlOptions'=>array('class'=>'operations'),
            ));
            $this->endWidget();
        ?>

        <?php 
            if(!Yii::app()->user->isGuest) $this->widget('UserMenu'); 
        ?>
        </div><!-- sidebar -->
    </div>
</div>
<?php $this->endContent(); ?>